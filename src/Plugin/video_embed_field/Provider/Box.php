<?php

namespace Drupal\video_embed_box\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * A Box Embed provider plugin.
 *
 * @VideoEmbedProvider(
 *   id = "box",
 *   title = @Translation("Box")
 * )
 *
 * @see https://developer.box.com/guides/embed/box-embed/
 */
class Box extends ProviderPluginBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    return [
      '#type' => 'video_embed_iframe',
      '#provider' => 'box',
      '#url' => sprintf('https://%s.app.box.com/embed/s/%s', $this->getCustomDomainFromInput($this->input), $this->getVideoId()),
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    preg_match(self::getUrlPattern(), $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }

  /**
   * URL Pattern for Box Embed videos to get custom domain and video id.
   *
   * Format: https://{custom_domain}.app.box.com(/embed)/s/{video id}
   *
   * @return string
   *   A regex expression pattern.
   */
  private static function getUrlPattern() {
    return '/^https?:\/\/(?<domain>[a-zA-Z0-9]*).app.box.com\/(embed\/)?s\/(?<id>[a-zA-Z0-9]*)$/';
  }

  /**
   * Get custom domain from input URL, format: {custom_domain}.app.box.com.
   */
  private function getCustomDomainFromInput($input) {
    preg_match(self::getUrlPattern(), $input, $matches);
    return isset($matches['domain']) ? $matches['domain'] : FALSE;
  }

}
